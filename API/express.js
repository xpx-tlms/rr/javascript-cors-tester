// 
// File: express.js
// Auth: Martin Burolla
// Date: 3/28/2022
// Desc: API test.
//

const express = require('express');
const cors = require('cors');

const app = express();

const API_PORT = 5150;
const API_VERSION = 'v1.0.0';

// Must state exposedHeaders in order for axios to read the value, 
// however x-api-version is visible in Postman.
var corsOptions = {
    origin: 'http://localhost:3000', // ACTUAL CORS HEADER NAME: Access-Control-Allow-Origin
    optionsSuccessStatus: 200,
    exposedHeaders: ['x-api-version'] // ACTUAL CORS HEADER NAME: Access-Control-Expose-Headers
}

// Middleware
app.use(express.json());
app.use(express.urlencoded());
app.use(cors());
app.use((req, res, next) => {
    res.setHeader('x-api-version', API_VERSION); // Add header on every request.
    next();
});

app.get('/person', cors(corsOptions), (req, res) => {
    console.log('GET: ' + req.url);
    let p = {firstName: "John", lastName: "Smith"};
    res.send(p);
});

app.put('/person', cors(corsOptions), (req, res) => {
    console.log('PUT: ' + req.url);
    res.send(req.body);
});

app.post('/person', cors(corsOptions), (req, res) => {
    console.log('POST: ' + req.url);
    res.send(req.body);
});

app.delete('/person', cors(corsOptions), (req, res) => {
    console.log('DEL: ' + req.url);
    res.send("ok");
});

app.listen(API_PORT, () => {
    console.log(`Express API is running on port: ${API_PORT}`);
});
