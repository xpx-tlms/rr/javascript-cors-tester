import './App.scss';
import axios from 'axios';
import { useEffect, useState } from 'react'

function App() {
  // Request
  let [body, setBody] = useState(null);
  let [verb, setVerb] = useState('GET');
  let [sendBody, setSendBody] = useState(false);
  let [sendReqHeader, setSendReqHeader] = useState(false);
  let [headerKey, setHeaderKey] = useState('Authorization');
  let [url, setUrl] = useState("http://localhost:5150/person");
  let [headerValue, setHeaderValue] = useState('Bearer TOKEN_GOES_HERE');
  // Response
  let [headers, setHeaders] = useState(null);
  let [requestBody, setRequestBody] = useState(JSON.stringify({firstName: "Joe"}));

  useEffect(()=> {

  }, []);
  
  const onFetchData = async () => {
    let res = null;
    const headers = {};
    if (sendReqHeader) { 
      headers["headers"] = {};
      headers["headers"][headerKey] = headerValue;
    }

    let body = null;
    if (sendBody) {
      body = JSON.parse(requestBody);
    }

    switch (verb) {
      case 'GET':
        res = await axios.get(url, headers);
      break;
      case 'PUT':
        res = await axios.put(url, body, headers);
      break;
      case 'POST':
        res = await axios.post(url, body , headers);
      break;
      case 'DELETE':
        res = await axios.delete(url, body, headers);
      break;
      default:
        console.log('HTTP verb not supported.');
    }

    setBody(JSON.stringify(res.data));
    // MUST specify Access-Control-Expose-Headers on the server.
    setHeaders(JSON.stringify(res.headers)); 
  }

  return (
    <div className="App">
      <div className="Top">
        <h2>Request</h2>
        <select className="selectVerb" onChange={(e)=> setVerb(e.target.value)}>
          <option value="GET">GET</option>
          <option value="PUT">PUT</option>
          <option value="POST">POST</option>
          <option value="DELETE">DELETE</option>
        </select>
        <input 
          className="inputUrl" 
          onChange={(e) => setUrl(e.target.value)}
          placeholder="http://localhost:5150/person"
        />
        <button onClick={() => onFetchData()} className="btn btn-primary">Send</button>
        <br/>
        <span>Headers</span>
        <br />
        <input onChange={(e)=> setHeaderKey(e.target.value)}className="reqHeaderKey" placeholder='Authorization'></input>
        <input onChange={(e)=> setHeaderValue(e.target.value)}className="reqHeaderValue" placeholder='Bearer TOKEN_GOES_HERE'></input>
        <input type="checkbox" onClick={() => setSendReqHeader(!sendReqHeader)}/> Send Header
        <br />

        <span>JSON Request Body</span> <input type="checkbox" onClick={() => setSendBody(!sendBody)}/> Send Body
        <br />
        <textarea 
          onChange={(e) => setRequestBody(e.target.value)} 
          className="textArea"
          placeholder={ "{\"firstName\" : \"Joe\"}" } >
        </textarea>
      </div>
      <div className="Bottom">
        <h2>Response</h2>
        <div className="Content">
          <span>Exposed Headers:</span>
          <div>
            {headers}
          </div>
          <br /> 
          <span>Body:</span>
          <div>
            {body}
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
